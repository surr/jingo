$(document).ready(function() {
  $("#wiki_upload").ajaxForm(function(response) {
    var RGX_FILENAME = /File uploaded to: <pre>([^<]+)<\/pre>/;
    var path = RGX_FILENAME.exec(response);
    if ( path && path[1] ) {
        insertTextAtCursor('![]('+path[1]+')');
        $('#upload').find('button')[0].click();
    }

    $('#upload_status').html(response);
  });
  $("input#wiki_file").change(function() {
    var filename = $(this).val().replace(/^.*[\\\/]/, '')
    var RGX_PAGE = /\/([^\/]+)\/edit\/?$/;
    var prefix = (RGX_PAGE.exec(window.location.href)||['PARSE_ERROR'])[1];
    $("input#rep_file_name").val(prefix + '_' + filename);
  });
});

function insertTextAtCursor(text) {
    var editor = $('.CodeMirror')[0].CodeMirror;
    var doc = editor.getDoc();
    var cursor = doc.getCursor();
    doc.replaceRange(text, cursor);
}
